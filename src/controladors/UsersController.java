package controladors;

import Connection.GetRequest;
import Connection.PostRequest;
import Connection.PutRequest;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import models.User;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.cell.PropertyValueFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import Connection.Requests;

public class UsersController implements Initializable{

    @FXML private Button afegirBTN;
    @FXML private Button editarBTN;
    @FXML private Button eliminarBTN;
    @FXML private Button nouBTN;
    @FXML private TextField idTF;
    @FXML private TextField nomTF;
    @FXML private TextField cognomsTF;
    @FXML private TextField emailTF;
    @FXML private PasswordField passwordTF;
    @FXML private TextField rolTF;
    @FXML private TableView<User> usuarisTL;
    @FXML private TableColumn idCL;
    @FXML private TableColumn nomCL;
    @FXML private TableColumn cognomsCL;
    @FXML private TableColumn emailCL;
    @FXML private TableColumn passwordCL;
    @FXML private TableColumn rolCL;
    ObservableList<User> data;
    private int posicionPersonaEnTabla;

    /**
     * Afegir usuari a la BBDD.
     * @author Gener Casanova
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void afegirUsuari(ActionEvent actionEvent) throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Hangul Daebak");
        alert.setHeaderText(null);
        alert.setContentText("Are you sure you want to add a new user?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            PostRequest post = new PostRequest("https://work.maximilianofernandez.net/api/users");
            post.add("name", nomTF.getText());
            post.add("surname", cognomsTF.getText());
            post.add("email", emailTF.getText());
            post.add("password", passwordTF.getText());
            post.add("role", rolTF.getText());
            String resposta = post.getRespuesta();
            System.out.println(rolTF.getText());
            System.out.println(resposta);
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION);
            alert2.setTitle("Hangul Daebak");
            alert2.setHeaderText(null);
            alert2.setContentText("User correctly created");
            alert2.showAndWait();
            this.inicializarTablaPersonas();
            nouUser();
        }else{
            alert.close();
        }
}




@FXML
    public void eliminarUsuari(ActionEvent actionEvent) throws Exception {
    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
    alert.setTitle("Hangul Daebak");
    alert.setHeaderText(null);
    alert.setContentText("Are you sure you want to delete this user?");

    Optional<ButtonType> result = alert.showAndWait();
    if (result.get() == ButtonType.OK) {
        String user = idTF.getText();
        Requests request = new Requests();
        request.deleteUser(user);
        URL url = null;
        Alert alert2 = new Alert(Alert.AlertType.INFORMATION);
        alert2.setTitle("Hangul Daebak");
        alert2.setHeaderText(null);
        alert2.setContentText("User correctly deleted");
        alert2.showAndWait();
        this.inicializarTablaPersonas();
        nouUser();
    }else{
        alert.close();
    }
    }


    /**
     * Mètode que permet a l'administrador  editar un usuari que es troba a la taula de usuaris.
     * @author Gener Casanova
     */
    @FXML
    public void editarUsuari(ActionEvent actionEvent) throws Exception {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Hangul Daebak");
        alert.setHeaderText(null);
        alert.setContentText("Are you sure you want to add a new user?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            String user = idTF.getText();
            PostRequest post = new PostRequest("https://work.maximilianofernandez.net/api/users/" + user + "}");
            post.add("name", nomTF.getText());
            post.add("surname", cognomsTF.getText());
            post.add("email", emailTF.getText());
            post.add("role", rolTF.getText());
            System.out.println(cognomsTF.getText());
            String resposta = post.getRespuesta();
            System.out.println(resposta);
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION);
            alert2.setTitle("Hangul Daebak");
            alert2.setHeaderText(null);
            alert2.setContentText("User correctly edited");
            alert2.showAndWait();
            this.inicializarTablaPersonas();
            nouUser();
        }else{
            alert.close();
        }
    }
    @FXML
    public void CancelAccount(ActionEvent event)throws Exception{
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Hangul Daebak");
        alert.setHeaderText("Users settings");
        alert.setContentText("Are you sure you want to cancel this account?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            String user = idTF.getText();
            PostRequest post = new PostRequest("https://work.maximilianofernandez.net/api/users/cancel/" + user + "}");
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION);
            alert2.setTitle("Hangul Daebak");
            alert2.setHeaderText(null);
            alert2.setContentText("Account cancelled");
            alert2.showAndWait();
            this.inicializarTablaPersonas();
        }else{
            alert.close();
        }
    }

    /**
     * Esborra els camps per crear un nou usuari.
     * @author Gener Casanova
     * @param actionEvent
     */

    @FXML
    public void nouUsuari(ActionEvent actionEvent) {
     nouUser();
    }

    public void nouUser(){
        idTF.setText("");
        idTF.setEditable(false);
        nomTF.setText("");
        cognomsTF.setText("");
        emailTF.setText("");
        rolTF.setText("");
        passwordTF.setText("");
        passwordTF.setDisable(false);
    }

    /**
     * Per omplir la taula am usuaris
     * @author Gener Casanova
     */
    private final ListChangeListener<User> selectorTablaPersonas =
            new ListChangeListener<User>() {
        @Override
        public void onChanged(ListChangeListener.Change<? extends User> c) {
            ponerPersonaSeleccionada();
        }
    };

    /**
     * Retorna lapersona seleccionada de la taula.
     * @author Gener Casanova
     * @return
     */
    public User getTablaPersonasSeleccionada() {
        if (usuarisTL != null) {
            List<User> tabla = usuarisTL.getSelectionModel().getSelectedItems();
            if (tabla.size() == 1) {
                final User competicionSeleccionada = tabla.get(0);
                return competicionSeleccionada;
            }
        }
        return null;
    }

    private void ponerPersonaSeleccionada() {
        final User persona = getTablaPersonasSeleccionada();
            posicionPersonaEnTabla = data.indexOf(persona);

            if (persona != null) {


                idTF.setText(String.valueOf(persona.getId()));
                idTF.setEditable(false);
                nomTF.setText(persona.getName());
            cognomsTF.setText(persona.getSurname());
            emailTF.setText(persona.getEmail());
            rolTF.setText(persona.getRole());
            passwordTF.setDisable(true);


            editarBTN.setDisable(false);
            eliminarBTN.setDisable(false);


        }
    }

    /**
     * Fa la petició get per obtenir les dades de l'usuari.
     * @author Gener Casanova
     * @throws Exception
     */

    @FXML
    public void  peticionHttpGet() throws Exception {
                BufferedReader bufferedReader;
                StringBuilder stringBuilder;
                String line;
                try {
                
                    URL url = new URL("https://work.maximilianofernandez.net/api/users");

                    HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                    conexion.setRequestMethod("GET");
                    conexion.setRequestProperty("Accept", "application/json");
                    conexion.setRequestProperty("connection", "Keep-Alive");
                    conexion.setRequestProperty("Authorization", "Bearer "+ LoginController.token);
                    conexion.connect();
                    if(conexion.getResponseCode()==200){
                        //obtiene respuesta
                        bufferedReader  = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
                        stringBuilder = new StringBuilder();
                        while ((line = bufferedReader.readLine()) != null)
                        {
                            stringBuilder.append(line);
                        }

                        usuarisTL.getItems().removeAll(data);


                        JSONArray dataArray  = new JSONArray(stringBuilder.toString());

                        for(int i = 0 ; i < dataArray.length(); i++){
                            JSONObject row = dataArray.getJSONObject(i);
                            data.add(new User(row.getInt("id"),row.getString("name"), row.getString("surname"),  row.getString("email"),row.getString("role") ));
                        }
                    }
                } catch (MalformedURLException ex) {
                    System.err.println("MalformedURLException: " +  ex.getMessage() );
                } catch (IOException ex) {
                    System.err.println( "IOException: " + ex.getMessage() );
                } catch (JSONException ex) {
                    System.err.println( "JSONException: " + ex.getMessage() );
        }
}
    private void inicializarTablaPersonas() {
        idCL.setCellValueFactory(new PropertyValueFactory<User,Integer>("id"));
        nomCL.setCellValueFactory(new PropertyValueFactory<User,String>("name"));
        cognomsCL.setCellValueFactory(new PropertyValueFactory<User,String>("surname"));
        emailCL.setCellValueFactory(new PropertyValueFactory<User,String>("email"));
        rolCL.setCellValueFactory(new PropertyValueFactory<User,String>("role"));

        data= FXCollections.observableArrayList();
        usuarisTL.setItems(data);
        try {
            peticionHttpGet();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Peer tornar al menú principal
     * @author Gener Casanova
     * @param event
     * @throws Exception
     */
    @FXML
    public void endarrera (ActionEvent event) throws Exception {
        Parent newparent = FXMLLoader.load(getClass().getResource("../vistes/MainMenuAdminView.fxml"));
        Scene newscene = new Scene(newparent);

        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(newscene);
        window.show();
    }
    /**
     * El mètode initialize es troba en tots els controlados de JavaFX. És el que s'executarà en obrir lel fitxer FXML.
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.inicializarTablaPersonas();

        //inicializamos datos
        final ObservableList<User> tablaPersonaSel = usuarisTL.getSelectionModel().getSelectedItems();
        tablaPersonaSel.addListener(selectorTablaPersonas);


    }



}





