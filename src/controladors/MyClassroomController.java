package controladors;

import Connection.GetRequest;
import Connection.PostRequest;
import Connection.PutRequest;
import com.google.gson.Gson;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import models.Classroom;
import models.Student;
import models.Student_Table;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Classe que exerceix de controlador de la pantalla on el professor veu els alumnes que té a la classe.També pot canviar el nom i la descripció de la classe.
 */

public class MyClassroomController implements Initializable {
    @FXML private TableView<Student_Table> studentsTL;
    @FXML private TableColumn idCL;
    @FXML private TableColumn nameCL;
    @FXML private TableColumn surnameCL;
    @FXML private TableColumn emailCL;
    @FXML private Label nameLB;
    @FXML private TextField classroomTF;
    @FXML private TextField descriptionTF;
    @FXML private Label updateLB;
    @FXML private Label errorLB;
    ObservableList<Student_Table> oListEstudiants;
    ArrayList<Student_Table> estudiants=new ArrayList<>();

    /**
     * Omple la taula fent la crida correponent rep el json amb les dades de la classe de del professor que fa la crida.
     * @author Gener Casanova
     * @throws Exception
     */
    public void loadTable() throws Exception{
        GetRequest get= new GetRequest();
        String resposta=get.PeticioGet("https://work.maximilianofernandez.net/api/professors/classroom");
        Gson gson = new Gson();
        Classroom c= gson.fromJson(resposta, Classroom.class);
        classroomTF.setText(c.getClassroom());
        descriptionTF.setText(c.getPresentation());
        System.out.println(c.getClassroom());
        ArrayList<Student> students = new ArrayList<>();
        students=c.getStudents();
        for(Student s: students){
        int ide=s.getId();
        String n=s.getUser().getName();
        String surnames=s.getUser().getSurname();
        String emails=s.getUser().getEmail();
        SimpleIntegerProperty id=new SimpleIntegerProperty(ide);
        SimpleStringProperty name=new SimpleStringProperty(n);
        SimpleStringProperty surname=new SimpleStringProperty(surnames);
        SimpleStringProperty email=new SimpleStringProperty(emails);
        Student_Table estudiant=new Student_Table(id,name,surname,email);
        estudiants.add(estudiant);
    }

        oListEstudiants = FXCollections.observableArrayList(estudiants);
        idCL.setCellValueFactory(new PropertyValueFactory<Student_Table,Integer>("id"));
        nameCL.setCellValueFactory(new PropertyValueFactory<Student_Table,String>("name"));
        surnameCL.setCellValueFactory(new PropertyValueFactory<Student_Table,Integer>("surname"));
        emailCL.setCellValueFactory(new PropertyValueFactory<Student_Table,Integer>("email"));
        studentsTL.setItems(oListEstudiants);

    }

    /**
     * Mètode que s'executa en fer clic al  botó i que permet actualitzar les dades de la classe ( nom i professor ).
     * @author Gener Casanova
     * @param event
     * @throws Exception
     */

    @FXML
    public void Update(ActionEvent event) throws Exception {
        if((classroomTF.getText().equals(""))||(descriptionTF.getText().equals(""))){
            errorLB.setText("Empty fields not permited");
            errorLB.setTextFill(Color.web("red"));
        }
        PostRequest post = new PostRequest("https://work.maximilianofernandez.net/api/professors");
        post.add("classroom",classroomTF.getText());
        post.add("presentation",descriptionTF.getText());
        String resposta=post.getRespuesta();
        updateLB.setText("Updated");
        updateLB.setTextFill(Color.web("green"));
        System.out.println(resposta);
    }
    /**
     * Porta de nou al menú principal.
     * @author Gener Casanova
     * @param event
     * @throws Exception
     */
    @FXML
    public void endarrera (ActionEvent event) throws Exception {
        Parent newparent = FXMLLoader.load(getClass().getResource("../vistes/MenuTeacherView.fxml"));
        Scene newscene = new Scene(newparent);

        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setTitle("Teacher menu");
        window.setScene(newscene);
        window.show();
    }

    /**
     * El mètode initialize es troba en tots els controlados de JavaFX. És el que s'executarà en obrir lel fitxer FXML.
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            loadTable();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
