package controladors;

import Connection.GetRequest;
import Connection.PostRequest;
import Connection.Requests;
import com.google.gson.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import models.*;
import org.jetbrains.annotations.NotNull;

import javax.swing.plaf.ColorUIResource;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Controlador que ens permet realitzar totes accions relacionades amb els tests ( crear i mostrar ).
 */
public class TestsController implements Initializable {
    @FXML
    private Label newQuestionsLBL;
    @FXML
    private Label pointsLBL;
    @FXML
    private Label descriptionLBL;
    @FXML
    private Label preguntaLBL;
    @FXML
    private Label option_1LBLsub;
    @FXML
    private Label option_2LBLsub;
    @FXML
    private Label option_3LBLsub;
    @FXML
    private Label option_1LBL;
    @FXML
    private Label option_2LBL;
    @FXML
    private Label option_3LBL;
    @FXML
    private Label soundsLike;
    @FXML
    private ComboBox<Gradebook> testsCB = new ComboBox<Gradebook>();
    @FXML
    private ComboBox<Student> combobox2 = new ComboBox<Student>();
    @FXML
    private ComboBox<Student> combobox = new ComboBox<Student>();
    @FXML
    private Button nextBTN;
    @FXML
    private Button sendBTN;
    @FXML
    private Pane panelVeure;
    @FXML
    private Pane panelCrear;
    @FXML
    private Pane panelPunts;
    @FXML
    private Button veureBtn;
    @FXML
    private Button nextQuestionBTN;
    @FXML
    private Button previousQuestionBTN;
    @FXML
    private Label correctaLbl;
    @FXML
    private Label incorrectaLbl;
    @FXML
    private Label incorrecta1Lbl;
    @FXML
    private TextField messageTF;

    /**
     * The Id quiz.
     */
    int idQuiz;
    /**
     * The Id student test.
     */
    int idStudentTest;
    /**
     * The Labels.
     */
    Label[]labels= new Label[3];
    /**
     * The Students.
     */
    ArrayList<Student> students =new ArrayList<>();
    /**
     * The Questions.
     */
    ArrayList<Question> questions = new ArrayList();
    /**
     * The Req.
     */
    Requests req=new Requests();
    /**
     * The Alpha.
     */
    ArrayList<AlphabetLetter> alpha =req.readJson();
    /**
     * The Classe.
     */
    String classe;
    /**
     * The Questiones.
     */
    ArrayList<QuestionResponses> questiones=new ArrayList<QuestionResponses>();


    /**
     * The Contador.
     */
    int contador =0;
    /**
     * The Contador 2.
     */
    int contador2=0;

    /**
     * Instantiates a new Tests controller.
     *
     * @throws Exception the exception
     */
    public TestsController() throws Exception {
    }

    /**
     * Crear tests.
     *
     * @param event the event
     * @throws Exception the exception
     */
    @FXML
    public void crearTests(ActionEvent event) throws Exception {

   omplirTest();

    }

    /**
     * Permet omplir inciar el panell per omplir els tests
     */
    public void omplirTest(){
        panelCrear.toFront();
        panelCrear.setVisible(true);
        panelVeure.setVisible(false);
        panelPunts.setVisible(false);
    }

    /**
     * Veure tests.
     *
     * @param event the event
     * @throws Exception the exception
     */
    @FXML
    public void veureTests(ActionEvent event) throws Exception {
    panelVeure.toFront();
    panelVeure.setVisible(true);
    panelCrear.setVisible(false);
    panelPunts.setVisible(false);


    }

    /**
     * Select letters.
     * @author Gener Casanova
     * @param event the event
     * @throws Exception the exception
     */
    @FXML
    /**
     * Imorimeix en pantalla les lletres seleccionades amb el teclat., controlant que la correcta i la incorrecta no estiguin repetides.
     */
    public void selectLetters (@NotNull ActionEvent event) throws Exception {
        soundsLike.setTextFill(Color.web("black"));
        Object node = event.getSource();
        Button b = (Button) node;
        String value = b.getText();
        String correcta = correctaLbl.getText();
        String incorrecta = incorrectaLbl.getText();
        String incorrecta2=incorrecta1Lbl.getText();
        AlphabetLetter lletra = null;
        for (AlphabetLetter al:alpha){

            if((al.getLetter()).equals(value)){
                lletra=al;
                System.out.println(lletra.getDescription());
            }
        }

                if (correcta.length()==0){
                correctaLbl.setText(value);
                soundsLike.setText(lletra.getDescription());
            }else if ((correcta.length() != 0) && (incorrecta.length() == 0)) {
            incorrectaLbl.setText(value);
        } else if ((correcta.length() != 0) && (incorrecta.length() != 0)) {
                    incorrecta1Lbl.setText(value);
                }
                if((correctaLbl.getText().equals(incorrectaLbl.getText()))||(correctaLbl.getText().equals(incorrecta1Lbl.getText()))){
                    soundsLike.setTextFill(Color.web("red"));
                    soundsLike.setText("Correct and incorrect letter cant be the same");
                    nextBTN.setDisable(true);
                }
    }


    /**
     * Puntuacions.
     *@author Gener Casanova
     * @param event the event
     * @throws Exception the exception
     */
    public void puntuacions (ActionEvent event) throws Exception {
    panelPunts.toFront();
    }

    /**
     * Envia pregunta.
     *@author Gener Casanova
     * @throws Exception Aquest controlador , agafa les lletres i crida al mètode guardarPreguntes, que les emmagatzema en un array.
     */
    @FXML
    public void enviaPregunta() throws Exception {
        contador++;
        soundsLike.setTextFill(Color.web("black"));
        String bona = correctaLbl.getText();
        String dolenta = incorrectaLbl.getText();
        String dolentas = incorrecta1Lbl.getText();
        String message=messageTF.getText();
        if (contador <10) {
            newQuestionsLBL.setText(Integer.toString(contador+1));
            Question q = Requests.guardarPreguntes(bona, dolenta, dolentas);
            questions.add(q);
            System.out.println("portes"+contador+"preguntes fetes");

           correctaLbl.setText("");
            incorrectaLbl.setText("");
            incorrecta1Lbl.setText("");
            soundsLike.setText("");
        } else if (contador==10) {
            newQuestionsLBL.setText("Test complet");
            Question q = Requests.guardarPreguntes(bona, dolenta, dolentas);
            questions.add(q);
            System.out.println("portes"+contador+"preguntes fetes");
            sendBTN.setDisable(false);
            nextBTN.setDisable(true);
            System.out.println("thas passat");
            for (Question obj : questions) {
                System.out.println("la primera és:"+obj.getOption_1());}

        }
        }

    /**
     * Instancia PostRequest i fa la peticio per emmagatzemmar a la BBDD el test. Passe els objectes dades a json i al revés amb la llibreria gson de Google.
     *@author Gener Casanova
     * @throws Exception the exception
     */
    @FXML
    public void SendQuiz() throws Exception {
        try {

            soundsLike.setText("");
            Timestamp date = new Timestamp(System.currentTimeMillis());
            int id = LoginController.idTeacher;

            String description = messageTF.getText();
            if(idStudentTest==0){
                soundsLike.setTextFill(Color.web("red"));
                soundsLike.setText("Select student");
            }else{
                Quiz quiz = new Quiz(idStudentTest, date, id, description, questions);
                Gson gson = new Gson();
                String json = "{ \"quiz\": " + gson.toJson(quiz) + " }";


                PostRequest post = new PostRequest("https://work.maximilianofernandez.net/api/quizzes/create");
                String respuesta = post.getRespuestaQuiz(json);
                System.out.println(respuesta);
                sendBTN.setDisable(true);
                soundsLike.setText("Test correctly loaded.");
                for(Question q: questions){
                    System.out.println(q.getOption_1());
                }
            }

        }catch (Exception e){
            soundsLike.setTextFill(Color.web("red"));
            soundsLike.setText("Not possible to load test");
        }

    }

    /**
     * Mètode per carregar els combobox que ens permeten elegir l'estudiant i el test realitzat per aquest.
     *@author Gener Casanova
     * @throws Exception the exception
     */
    public void LoadCombo() throws Exception {
            GetRequest get = new GetRequest();
              classe = get.PeticioGet("https://work.maximilianofernandez.net/api/professors/classroom");
            System.out.println(classe);
            Gson gson = new Gson();
            Classroom classroom = gson.fromJson(classe, Classroom.class);
            ArrayList<Student> students = classroom.getStudents();
            ObservableList<Student> oListNoms = FXCollections.observableArrayList(students);
             combobox.setItems(oListNoms);
            combobox2.setItems(oListNoms);
        }

    /**
     * Load combo 2.
     *@author Gener Casanova
     * @param id the id
     * @throws Exception the exception
     */
    public void LoadCombo2(int id) throws Exception {
        GetRequest get = new GetRequest();
        String peticio = get.PeticioGet("https://work.maximilianofernandez.net/api/gradebooks/"+id);
        System.out.println(peticio);
        Gson gson = new Gson();
        Student_Combobox student = gson.fromJson(peticio, Student_Combobox.class);
        ArrayList<Gradebook> gradebooks = student.getGradebooks();
        for(Gradebook model : gradebooks) {
            System.out.println(model.getScore());
        }
        ObservableList<Gradebook> oListGradebooks = FXCollections.observableArrayList(gradebooks);
        testsCB.setItems(oListGradebooks);


    }

    /**
     * Detecta canvis en el combobox(segons l'usuari seleccionat carrega els seus examens ).
     * @param event
     */
    @FXML
    private void getTest (ActionEvent event){
        testsCB.getSelectionModel().selectedItemProperty().addListener((obs, oldVal, newval) -> {
            if (newval != null) {
                idQuiz = newval.getQuiz_id();



            }

        });
    }

    /**
     * Mostra els tests en pantalla
     *@author Gener Casanova
     * @param event the event
     * @throws Exception the exception
     */
    @FXML
    public void ShowQuiz(ActionEvent event) throws Exception {
        contador2=0;
        nextQuestionBTN.setDisable(false);
        previousQuestionBTN.setDisable(true);
        option_1LBL.setText("");
        option_2LBL.setText("");
        option_3LBL.setText("");
        option_1LBLsub.setText("");
        option_2LBLsub.setText("");
        option_3LBLsub.setText("");
        option_1LBLsub.setStyle("");
        option_2LBLsub.setStyle("");
        option_3LBLsub.setStyle("");
        GetRequest get=new GetRequest();
        String peticio= get.PeticioGet("https://work.maximilianofernandez.net/api/quizzes/"+idQuiz);
        System.out.println(peticio);
        System.out.println(classe);
        Gson gson = new Gson();
        Student estudiant=null;

        QuizResponses quiz = gson.fromJson(peticio,QuizResponses.class);
         questiones = quiz.getQuestions();
         int points=quiz.getGradebook().getScore();
         pointsLBL.setText(Integer.toString(points));
        option_1LBL.setText(questiones.get(contador2).getOption_1().getLetter());
        option_2LBL.setText(questiones.get(contador2).getOption_2().getLetter());
        option_3LBL.setText(questiones.get(contador2).getOption_3().getLetter());
        String description1=(questiones.get(contador2).getOption_1().getDescription());
        String description2=(questiones.get(contador2).getOption_2().getDescription());
        String description3= (questiones.get(contador2).getOption_3().getDescription());
        String[]descriptions= new String[3];
        descriptions[0]=description1;
        descriptions[1]=description2;
        descriptions[2]=description3;
        int answer= (questiones.get(contador2).getAnswer())-1;
        int correct= (questiones.get(contador2).getCorrect())-1;
        preguntaLBL.setText(Integer.toString(contador2+1));
        Label[]labels= new Label[3];
        labels[0]=option_1LBLsub;
        labels[1]=option_2LBLsub;
        labels[2]=option_3LBLsub;
        descriptionLBL.setText(descriptions[correct]);
        if (answer == correct) {
            labels[answer].setStyle("-fx-background-color: cyan;");
            labels[answer].setText("10 points");
        } else {
            labels[0] = option_1LBLsub;
            labels[1] = option_2LBLsub;
            labels[2] = option_3LBLsub;
            labels[answer].setStyle("-fx-background-color: yellow;");
            labels[answer].setText("Resposta alumne/a");
            labels[correct].setStyle("-fx-background-color: Green;");
            labels[correct].setText("Resposta correcta");
        }
        System.out.println(contador2);
    }

    /**
     * Detecta canvis en el combobox.
     * @param event
     */
    @FXML
    private void comboAction(ActionEvent event) {


       Student s =combobox.getSelectionModel().selectedItemProperty().get();
       idStudentTest=s.getId();
                System.out.println(idStudentTest);



    }

    /**
     * Detecta canvis en el combobox.
     * @param event
     * @author Gener Casanova
     */
    @FXML
    private void comboActionShowTests(ActionEvent event) {
        combobox2.getSelectionModel().selectedItemProperty().addListener((obs, oldVal, newval) -> {
            if (newval != null) {
                idStudentTest = newval.getId();
                System.out.println(idStudentTest);
                try {
                    LoadCombo2(idStudentTest);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        });
    }

    /**
     * Carrega en pantalla la pregunta següent del test realitzar per l'alumne.
     *@author Gener Casanova
     * @param event the event
     */
    @FXML
    public void nextQuestion (ActionEvent event) {
        contador2++;
        int llargada = questiones.size();
        System.out.println(llargada);
        if (contador2 < llargada) {
            preguntaLBL.setText(Integer.toString(contador2+1));
            previousQuestionBTN.setDisable(false);
                option_1LBL.setText("");
                option_2LBL.setText("");
                option_3LBL.setText("");
                option_1LBLsub.setText("");
                option_2LBLsub.setText("");
                option_3LBLsub.setText("");
                option_1LBLsub.setStyle("");
                option_2LBLsub.setStyle("");
                option_3LBLsub.setStyle("");
                option_1LBL.setText(questiones.get(contador2).getOption_1().getLetter());
                option_2LBL.setText(questiones.get(contador2).getOption_2().getLetter());
                option_3LBL.setText(questiones.get(contador2).getOption_3().getLetter());
                int answer = (questiones.get(contador2).getAnswer()) - 1;
                int correct = (questiones.get(contador2).getCorrect()) - 1;
            String description1=(questiones.get(contador2).getOption_1().getDescription());
            String description2=(questiones.get(contador2).getOption_2().getDescription());
            String description3= (questiones.get(contador2).getOption_3().getDescription());
            String[]descriptions= new String[30];
            descriptions[0]=description1;
            descriptions[1]=description2;
            descriptions[2]=description3;
            descriptionLBL.setText(descriptions[correct]);
                if (answer == correct) {
                    labels[answer].setStyle("-fx-background-color: cyan;");
                    labels[answer].setText("10 points");
                } else {
                    labels[0] = option_1LBLsub;
                    labels[1] = option_2LBLsub;
                    labels[2] = option_3LBLsub;
                    labels[answer].setStyle("-fx-background-color: yellow;");
                    labels[answer].setText("Student answer");
                    labels[correct].setStyle("-fx-background-color: Green;");
                    labels[correct].setText("Correct answer");
                }

            }if(contador2==(llargada-1)){
            nextQuestionBTN.setDisable(true);
        }
        System.out.println(contador2);
        }

    /**
     * Igual que nextQuestion però carrega la pregunta anterior.
     * @author Gener Casanova
     * @param event the event
     */
    @FXML

    public void previousQuestion (ActionEvent event) {
        System.out.println(contador2);
        System.out.println(contador2);
        int llargada = questiones.size();

        if (contador2 > 0) {

            contador2--;
            preguntaLBL.setText(Integer.toString(contador2+1));
            nextQuestionBTN.setDisable(false);
            option_1LBL.setText("");
            option_2LBL.setText("");
            option_3LBL.setText("");
            option_1LBLsub.setText("");
            option_2LBLsub.setText("");
            option_3LBLsub.setText("");
            option_1LBLsub.setStyle("");
            option_2LBLsub.setStyle("");
            option_3LBLsub.setStyle("");
            option_1LBL.setText(questiones.get(contador2).getOption_1().getLetter());
            option_2LBL.setText(questiones.get(contador2).getOption_2().getLetter());
            option_3LBL.setText(questiones.get(contador2).getOption_3().getLetter());
            int answer = (questiones.get(contador2).getAnswer()) - 1;
            int correct = (questiones.get(contador2).getCorrect()) - 1;
            String description1=(questiones.get(contador2).getOption_1().getDescription());
            String description2=(questiones.get(contador2).getOption_2().getDescription());
            String description3= (questiones.get(contador2).getOption_3().getDescription());
            String[]descriptions= new String[30];
            descriptions[0]=description1;
            descriptions[1]=description2;
            descriptions[2]=description3;
            descriptionLBL.setText(descriptions[correct]);
            if (answer == correct) {
                labels[answer].setStyle("-fx-background-color: cyan;");
                labels[answer].setText("Resposta encertada");
            } else {
                labels[0] = option_1LBLsub;
                labels[1] = option_2LBLsub;
                labels[2] = option_3LBLsub;
                labels[answer].setStyle("-fx-background-color: yellow;");
                labels[answer].setText("Resposta alumne/a");
                labels[correct].setStyle("-fx-background-color: Green;");
                labels[correct].setText("Resposta correcta");
            }

        }if (contador2==0){
            previousQuestionBTN.setDisable(true);
        }
    }

    /**
     * Ens porta a la pantalla anterior.
     * @author Gener Casanova
     * @param event the event
     * @throws Exception the exception
     */
    @FXML
    public void endarrera (ActionEvent event) throws Exception {
        System.out.println(idStudentTest);
        Parent newparent = FXMLLoader.load(getClass().getResource("../vistes/MenuTeacherView.fxml"));
        Scene newscene = new Scene(newparent);

        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(newscene);
        window.show();
    }

    /**
     * Edit question.
     *
     * @param event the event
     */
    @FXML
    public void editQuestion (ActionEvent event){
        correctaLbl.setText("");
        incorrectaLbl.setText("");
        incorrecta1Lbl.setText("");
        soundsLike.setText("");
        nextBTN.setDisable(false);
    }


        @Override
    public void initialize(URL location, ResourceBundle resources) {
      omplirTest();
        try {
            LoadCombo();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
