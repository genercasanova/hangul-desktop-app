package controladors;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import Connection.Requests;
import java.io.IOException;

import static Connection.Requests.PeticioGet;

public class MainMenuController {
    /**
     * Ens porta a la pantalla de gestió de usuaris (admin).
     * @param event
     * @author Gener Casanova
     * @throws IOException
     */
    @FXML
    public void accedirUsuaris(ActionEvent event) throws IOException {
        Parent newparent = FXMLLoader.load(getClass().getResource("../vistes/UsersView.fxml"));
        Scene newscene = new Scene(newparent);

        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.getIcons().add(new Image("file:///C:/Users/usuari/IdeaProjects/provant/imatges/bandera.png"));
        window.setTitle("Users settings");
        window.setScene(newscene);
        window.show();

    }
    @FXML
    public void goToProfile(ActionEvent event) throws IOException {
        Parent newparent = FXMLLoader.load(getClass().getResource("../vistes/ConfigUser.fxml"));
        Scene newscene = new Scene(newparent);

        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.getIcons().add(new Image("file:///C:/Users/usuari/IdeaProjects/provant/imatges/bandera.png"));
        window.setTitle("My profile");
        window.setScene(newscene);
        window.show();

    }
    @FXML
    public void goToClassrooms(ActionEvent event) throws IOException {

        Parent newparent = FXMLLoader.load(getClass().getResource("../vistes/ClassroomsStudents.fxml"));
        Scene newscene = new Scene(newparent);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.getIcons().add(new Image("file:///C:/Users/usuari/IdeaProjects/provant/imatges/bandera.png"));
        window.setTitle("Students and classrooms");
        window.setScene(newscene);
        window.show();
    }
    /**
     * Tanca la sessió amb la corresponent crida al servidor ( logout ).
     * @author Gener Casanova
     * @param event
     * @throws Exception
     */
    @FXML
    public void tancarSessio(ActionEvent event) throws Exception {
        Requests requests=new Requests();
        requests.tancarLaSessio();
        Parent newparent = FXMLLoader.load(getClass().getResource("../vistes/LoginView.fxml"));
        Scene newscene = new Scene(newparent);

        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(newscene);
        window.show();
    }
}
