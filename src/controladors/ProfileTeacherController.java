package controladors;


import Connection.GetRequest;
import Connection.PostRequest;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.json.JSONObject;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Classe controlador del menú que permet que l'usuari ( professor oadministrador modifiqui el seu perfil).
 */
public class ProfileTeacherController implements Initializable {

    @FXML private TextField nameTF;
    @FXML private TextField surnameTF;
    @FXML private TextField roleTF;
    @FXML private TextField emailTF;
    @FXML private TextField passwordTF;
    @FXML private TextField new_passwordTF;
    @FXML private Label passwordLB;
    @FXML private Label updateLB;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        String resposta="";
        GetRequest get = new GetRequest();
        String nom="";
        String cognom="";
        String rol="";
        String email="";
        try {
            resposta = get.PeticioGet("https://work.maximilianofernandez.net/api/profile");
            JSONObject json = new JSONObject(resposta);
            nom= json.getJSONObject("success").getString("name");
            cognom=json.getJSONObject("success").getString("surname");;
            rol=json.getJSONObject("success").getString("role");
            email=json.getJSONObject("success").getString("email");

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(resposta);

        nameTF.setText(nom);
        surnameTF.setText(cognom);
        emailTF.setText(email);
        emailTF.setEditable(false);
        roleTF.setText(rol);
        roleTF.setEditable(false);


    }

    @FXML
    private void UpdatePassword (ActionEvent event) {
        String password = passwordTF.getText();
        String new_password = new_passwordTF.getText();
        if(password.equals(new_password)||password.equals("")||(new_password.equals(""))){
            passwordLB.setText("Passwords are the same or field is empty");
            passwordLB.setTextFill(Color.web("red"));
        }else {
            try {
                PostRequest post = new PostRequest("https://work.maximilianofernandez.net/api/profile/newpass");
                String id = String.valueOf(LoginController.idTeacher);
                post.add("id_user", id);
                post.add("old_password", password);
                post.add("new_password", new_password);
                String resposta = post.getRespuesta();
                System.out.println(resposta);
                passwordLB.setText("Password changed");
                passwordLB.setTextFill(Color.web("green"));
            }catch(Exception e){
                passwordLB.setText("Error");
                passwordLB.setTextFill(Color.web("red"));
            }
        }
    }
    @FXML
    public void UpdateProfile (ActionEvent event) throws Exception {
        if(nameTF.getText().equals("")){
            updateLB.setText("Name and suanrme can't be empty");
            updateLB.setTextFill(Color.web("red"));
        }if(surnameTF.getText().equals("")){
            updateLB.setText("Name and suanrme can't be empty");
            updateLB.setTextFill(Color.web("red"));
        } else{
            PostRequest post= new PostRequest("https://work.maximilianofernandez.net/api/profile");
            post.add("name",nameTF.getText());
            post.add("surname",surnameTF.getText());
            String resposta=post.getRespuesta();
            updateLB.setText("Profile updated");
            updateLB.setTextFill(Color.web("green"));
        }
    }

    @FXML
    public void endarrera (ActionEvent event) throws Exception {
        Parent newparent = FXMLLoader.load(getClass().getResource("../vistes/MenuTeacherView.fxml"));
        Scene newscene = new Scene(newparent);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.getIcons().add(new Image("file:///C:/Users/usuari/IdeaProjects/provant/imatges/bandera.png"));
        window.setTitle("Teacher menu");
        window.setScene(newscene);
        window.show();
    }
}
