package controladors;

import Connection.GetRequest;
import Connection.PostRequest;
import Connection.PutRequest;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import models.*;

import javafx.event.ActionEvent;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controlador perla pantalla que serveix per canviar els usuaris de classe
 */
public class ClassroomsStudentsController implements Initializable  {

    @FXML private ComboBox<Classroom> classroomsCB2;
    @FXML private ComboBox<Classroom> classroomsCB;
    @FXML private TableView<Student_Table> studentsTL;
    @FXML private TableColumn idCL;
    @FXML private TableColumn nameCL;
    @FXML private TableColumn surnameCL;
    @FXML private Label confirmationLB;
    @FXML private ImageView okIMG;

    ArrayList<Student> students;
    ObservableList<Classroom> oListClass;
    ObservableList<Student_Table> oListEstudiants;
    List<Classroom> posts=new ArrayList<Classroom>();
    ArrayList<Professor> professors=new ArrayList<>();
    List<User>users;
    ArrayList<Student_Table> estudiants=new ArrayList<>();
    int id;

    /**Aquest mètode rellena el combobox amb totes les classes
     * @author Gener Casanova
     * @throws Exception
     */
    public void LoadCombo() throws Exception {

        GetRequest get = new GetRequest();

        String classe = get.PeticioGet("https://work.maximilianofernandez.net/api/classrooms");
        System.out.println(classe);
        Gson gson = new Gson();

        studentsTL.setPlaceholder(new Label("First, you must select a classroom"));
        Type listType = new TypeToken<List<Classroom>>() {
        }.getType();

        posts = gson.fromJson(classe, listType);
        oListClass = FXCollections.observableArrayList(posts);
        classroomsCB.setItems(oListClass);
        classroomsCB2.setItems(oListClass);

    }
    /**
     * Al seleccionar una classe del combobox rellena la taula amb els estudiant de la classe seleccionada.
     * @author Gener Casanova
     */
    @FXML
    public void onChangeCombo() throws Exception {

        estudiants.clear();
        oListEstudiants = FXCollections.observableArrayList(estudiants);
        studentsTL.setItems(oListEstudiants);
        Classroom cl = classroomsCB.getValue();
        for (Classroom c : posts) {
            if (c.getClassroom().equals(cl.getClassroom())) {
                students = c.getStudents();

            }}
            for(Student s: students){
            int ide=s.getId();
            String n=s.getUser().getName();
            String c=s.getUser().getSurname();
            SimpleIntegerProperty id=new SimpleIntegerProperty(ide);
            SimpleStringProperty name=new SimpleStringProperty(n);
            SimpleStringProperty surname=new SimpleStringProperty(c);


            Student_Table estudiant=new Student_Table(id,name,surname);
            estudiants.add(estudiant);
        }

        oListEstudiants = FXCollections.observableArrayList(estudiants);
        idCL.setCellValueFactory(new PropertyValueFactory<Student_Table,Integer>("id"));
        nameCL.setCellValueFactory(new PropertyValueFactory<Student_Table,String>("name"));
        surnameCL.setCellValueFactory(new PropertyValueFactory<Student_Table,Integer>("surname"));
        studentsTL.setItems(oListEstudiants);
        confirmationLB.setText("");
        okIMG.setVisible(false);
    }

    /**
     * Mètode al premer el botó fa la crida necessària al servidor per canviarl'alumne de classe, passant els paràmetres amblaid del nou professor i la id de l'estudiant.
     * @author Gener Casanova
     */
    @FXML
    public void changeClassroom() {
        boolean isMyComboBoxEmpty = (classroomsCB2.getValue() == null);

        if(isMyComboBoxEmpty==true){
            confirmationLB.setText("You have to select a classroom");
            confirmationLB.setTextFill(Color.web("red"));
        }
            Classroom cl1 = classroomsCB.getValue();
            String nom1= cl1.getClassroom();
            Classroom cl = classroomsCB2.getValue();
            String nom = cl.getClassroom();
            System.out.println(nom);

            if(nom1.equals(nom)){
                confirmationLB.setText("Old and new classroom can't be the same");
                confirmationLB.setTextFill(Color.web("red"));
            }else{
            int ids = cl.getProfessor().getId();
            System.out.println(ids);
            String id_profe = String.valueOf(ids);
            String id_student = String.valueOf(id);
            try {
                PostRequest post = new PostRequest("https://work.maximilianofernandez.net/api/classrooms/update");
                post.add("professor_id", id_profe);
                post.add("student_id", id_student);
                String resposta = post.getRespuesta();
                System.out.println(resposta);
                confirmationLB.setText("Student's classroom correctly changed");
                confirmationLB.setTextFill(Color.web("green"));
                okIMG.setVisible(true);
                LoadCombo();
            } catch (Exception e ) {
                confirmationLB.setText("Not possible to change student classroom");
            }}}


    /**Permet seleccionar un nou estudiant de la taula.
     * @author Gener Casanova
     */
    @FXML
    public void takeRow()  {
        try {
            Student_Table stu = studentsTL.getSelectionModel().selectedItemProperty().get();
            id = (stu.getId());
            System.out.println(id);
            classroomsCB2.setDisable(false);
        }catch (Exception e){
            confirmationLB.setText("First you must select a classroom");
            confirmationLB.setTextFill(Color.web("red"));
        }
      }

    /**
     * Tornem al menú principal.
     * @author Gener Casanova
     * @param event
     * @throws Exception
     */
    @FXML
    public void endarrera (ActionEvent event) throws Exception {
        Parent newparent = FXMLLoader.load(getClass().getResource("../vistes/MainMenuAdminView.fxml"));
        Scene newscene = new Scene(newparent);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(newscene);
        window.show();
    }
    /**
     * El mètode initialize es troba en tots els controlados de JavaFX. És el que s'executarà en obrir lel fitxer FXML.
     * @param location
     * @param resources
     */

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            LoadCombo();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}
