package controladors;


import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import java.net.URL;
import java.util.ResourceBundle;
import Connection.PostRequest;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import models.User;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import Connection.Requests;

import static Connection.Requests.PeticioGet;

public class TeacherMainController  implements Initializable {
    /**
     * Tanca la sessió i ens porta a la pantalla de Login.
     * @param event
     * @throws Exception
     * @author Gener Casanova
     */
    @FXML
    public void tancarSessio(ActionEvent event) throws Exception {
        Requests requests=new Requests();
        requests.tancarLaSessio();
        Parent newparent = FXMLLoader.load(getClass().getResource("../vistes/LoginView.fxml"));
        Scene newscene = new Scene(newparent);

        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(newscene);
        window.show();
    }

    /**
     *
     * @param event
     * @throws Exception
     */
    @FXML
    public void accedirAreaTests(ActionEvent event) throws Exception {

        Parent newparent = FXMLLoader.load(getClass().getResource("../vistes/AreaTests.fxml"));
        Scene newscene = new Scene(newparent);
        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setTitle("Area tests");
        window.setScene(newscene);
        window.show();

    }
        @FXML
        public void yourClassroom(ActionEvent event) throws IOException {
        Parent newparent = FXMLLoader.load(getClass().getResource("../vistes/TeacherClassroom.fxml"));
        Scene newscene = new Scene(newparent);

        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.getIcons().add(new Image("file:///C:/Users/usuari/IdeaProjects/provant/imatges/bandera.png"));
        window.setTitle("My classroom");
        window.setScene(newscene);
        window.show();
    }
    @FXML
    public void YourProfile(ActionEvent event) throws IOException {
        Parent newparent = FXMLLoader.load(getClass().getResource("../vistes/ConfigUserTeacher.fxml"));
        Scene newscene = new Scene(newparent);

        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.getIcons().add(new Image("file:///C:/Users/usuari/IdeaProjects/provant/imatges/bandera.png"));
        window.setTitle("Profile");
        window.setScene(newscene);
        window.show();

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
