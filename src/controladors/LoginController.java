package controladors;


import Connection.GetRequest;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import Connection.PostRequest;
import models.AlphabetLetter;
import org.json.JSONArray;
import org.json.JSONObject;
import static Connection.Requests.PeticioGet;
import static Connection.Requests.readJson;


public class LoginController {
    public static String respuesta;
    public static String token;
    public static int idTeacher;


    @FXML
    private Label labelError;
    @FXML
    private Button closeButto;
    @FXML
    private TextField textCorreu;
    @FXML
    private TextField textContrasenya;
    @FXML
    private Button butoCancelar;
    @FXML
    private Button butoEnviar;
    @FXML
    private TextField correuTF;

    @FXML
    public boolean isEmail(String correo) {
        Pattern pat = null;
        Matcher mat = null;
        pat = Pattern.compile("^([0-9a-zA-Z]([_.w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-w]*[0-9a-zA-Z].)+([a-zA-Z]{2,9}.)+[a-zA-Z]{2,3})$");
        mat = pat.matcher(correo);
        if (mat.find()) {
            System.out.println("[" + mat.group() + "]");
            return true;
        } else {
            return false;
        }
    }

    /**
     * Comprova que la contrasenya tingui el format demanat.
     * @author Gener Casanova
     * @param contrasenya
     * @return
     */
    @FXML
    public boolean contrasenyaCorrecta(String contrasenya) {
        Pattern pat = null;
        Matcher mat = null;
        pat = Pattern.compile("((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{8,40})");
        mat = pat.matcher(contrasenya);
        if (mat.find()) {
            System.out.println("contrasenya ok");
            return true;
        } else {
            System.out.println("contrasenya incorrecta");
            return false;
        }
    }

    /**
     * Després de comprovar que els les dades introduides son correctes les envia amb  la petició Post i després per peticio Get rep el json amb la el toke, la ID i el rol de l'usuari. A partir d'aquí el porta a una pantalla o una altra.
     *
     * @param event
     * @throws Exception
     */
    @FXML
    public void gotoCreateCategory(ActionEvent event) throws Exception {
        String correu = textCorreu.getText();
        String contrasenya = textContrasenya.getText();
             /* if (correu.length() == 0) {
                    labelError.setText("Introdueix el correu");
                    textCorreu.requestFocus();
                } else if (contrasenya.length() == 0) {
            labelError.setText("Introdueix la contrasenya");
            textContrasenya.requestFocus();
        } else if ((isEmail(correu) == true) && (contrasenyaCorrecta(contrasenya) == false)) {
            labelError.setText("la contrasenya està mal escrita");
            textContrasenya.requestFocus();
        } else if ((isEmail(correu) == false) && (contrasenyaCorrecta(contrasenya) == false)) {
            labelError.setText("El correu i la contrasenya estan mal escrits");
            textCorreu.requestFocus();
        } else if ((isEmail(correu) == false) && (contrasenyaCorrecta(contrasenya) == true)) {
            labelError.setText("El correu és incorrecte");
            textCorreu.requestFocus();
        } else if ((isEmail(correu) == true) && (contrasenyaCorrecta(contrasenya) == true)) {
            labelError.setText("Usuari inexistent");
            textCorreu.requestFocus();
*/

        PostRequest post = new PostRequest("https://work.maximilianofernandez.net/api/login");
        post.add("email", textCorreu.getText());
        post.add("password", textContrasenya.getText());
        respuesta = post.getRespuestaLogin();
        JSONObject jason = new JSONObject(respuesta);
        token = jason.getJSONObject("success").getString("token");

        System.out.println(token);
        String tipusUser = PeticioGet("https://work.maximilianofernandez.net/api/profile?email="+ correu);
        JSONObject json = new JSONObject(tipusUser);
        String rol= json.getJSONObject("success").getString("role");
        idTeacher=json.getJSONObject("success").getInt("id");

        System.out.println(rol);
        System.out.println(idTeacher);

        if (rol.equals("A")) {
            Stage stage = new Stage();
            stage.getIcons().add(new Image("file:///C:/Users/usuari/IdeaProjects/provant/imatges/bandera.png"));
            stage.setTitle("Espai Administrador");
            Pane myPane = null;
            myPane = FXMLLoader.load(getClass().getResource("../vistes/MainMenuAdminView.fxml"));
            Scene scene = new Scene(myPane);
            stage.setScene(scene);

            Stage stage2 = (Stage) butoEnviar.getScene().getWindow();
            stage2.close();


            stage.show();
        } else if (rol.equals("P")) {
            Stage stage = new Stage();
            stage.getIcons().add(new Image("file:///C:/Users/usuari/IdeaProjects/provant/imatges/bandera.png"));
            stage.setTitle("Teacher menu");
            Pane myPane = null;
            myPane = FXMLLoader.load(getClass().getResource("../vistes/MenuTeacherView.fxml"));
            Scene scene = new Scene(myPane);
            stage.setScene(scene);

            Stage stage2 = (Stage) butoEnviar.getScene().getWindow();
            stage2.close();


            stage.show();
        }

    }




    /**
     * @param event
     * @throws IOException
     */

    @FXML
    public void obrirPopup(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(
                LoginController.class.getResource("../Login/popUpRecordatori.fxml"));
        stage.setScene(new Scene(root));
        stage.setTitle("Forgot password");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(
                ((Node) event.getSource()).getScene().getWindow());
        stage.show();
    }

    @FXML
    public void CloseWindow(ActionEvent event) throws IOException {
        Stage stage = (Stage) butoCancelar.getScene().getWindow();
        stage.close();
    }

    /**
     * Mètode que fa la crida al servidor perquè enviï un correo a l'adreçaemail associada.
     * @author Gener Casanova
     * @param event
     * @throws Exception
     */
    @FXML
    public void SendEmail(ActionEvent event) throws Exception {
        String correu = correuTF.getText();
        if (correu.equals("")) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Hangul daebak");
            alert.setHeaderText(null);
            alert.setContentText("Password field is empty");
            alert.showAndWait();
        } else {
            try {

                GetRequest get = new GetRequest();
                String resposta = get.PeticioGet("https://work.maximilianofernandez.net/api/lostpass/" + correu);
                System.out.println(resposta);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Hangul daebak");
                alert.setHeaderText(null);
                alert.setContentText("New password has been send to your email.");
                alert.showAndWait();
                Stage stage = (Stage) butoCancelar.getScene().getWindow();
                stage.close();
            } catch (Exception e) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Hangul daebak");
                alert.setHeaderText(null);
                alert.setContentText("This email is not valid");
                correuTF.setText("");
            }


        }

    }
}






