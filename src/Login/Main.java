package Login;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("../vistes/LoginView.fxml"));
        primaryStage.getIcons().add(new Image("file:///C:/Users/usuari/IdeaProjects/provant/imatges/bandera.png"));
        primaryStage.setTitle("Login");
        primaryStage.setScene(new Scene(root, 535, 360));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
