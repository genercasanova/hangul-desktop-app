package models;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Classe estudiant amb 2 constructors.Té SimpleProperty per poder mostrar els objectes a les taules de JavaFX
 */
public class Student_Table {
    public SimpleIntegerProperty id;
    public SimpleStringProperty name;
    public SimpleStringProperty surname;
    public SimpleStringProperty email;

    /**
     *
     * @param id
     * @param name
     * @param surname
     * @param email
     */
    public Student_Table(SimpleIntegerProperty id, SimpleStringProperty name, SimpleStringProperty surname,SimpleStringProperty email ) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email=email;
    }

    /**
     *
     * @param id
     * @param name
     * @param surname
     */

    public Student_Table(SimpleIntegerProperty id, SimpleStringProperty name, SimpleStringProperty surname) {
        this.id = id;
        this.name = name;
        this.surname = surname;

    }

    public int getId() {
        return id.get();
    }

    public SimpleIntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getSurname() {
        return surname.get();
    }

    public SimpleStringProperty surnameProperty() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname.set(surname);
    }

    public String getEmail() {
        return email.get();
    }

    public SimpleStringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }
}
