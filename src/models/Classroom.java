package models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
/**
 *  //Donem el nom de l'atribut amb què rebem el gson perquè el pugui deserialitzar correctament.
 */
public class Classroom{

    @SerializedName(value = "classroom", alternate = "name")
    String classroom;
    @SerializedName(value = "presentation", alternate = "description")
    String presentation;
    Professor professor;
    int totalStudent;
    ArrayList<Student> students;

    /**
     *@author Gener Casanova
     * @param classroom
     * @param presentation
     * @param professor
     * @param totalStudent
     * @param students
     */
    public Classroom(String classroom, String presentation, Professor professor, int totalStudent, ArrayList<Student> students) {
        this.classroom = classroom;
        this.presentation = presentation;
        this.professor = professor;
        this.totalStudent = totalStudent;
        this.students = students;
    }

    public Classroom(){

    }

    public String getClassroom() {
        return classroom;
    }

    public void setClassroom(String classroom) {
        this.classroom = classroom;
    }

    public String getPresentation() {
        return presentation;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public int getTotalStudent() {
        return totalStudent;
    }

    public void setTotalStudent(int totalStudent) {
        this.totalStudent = totalStudent;
    }

    public  ArrayList<Student> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }
    @Override
    public String toString() {
        return this.getClassroom();
}
}