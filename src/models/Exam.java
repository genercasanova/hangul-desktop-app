package models;

import java.util.List;

public class Exam {
    int idUser;
    List<Result> resultats;

    public Exam(int idUser, List<Result> resultats) {
        this.idUser = idUser;
        this.resultats = resultats;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public List<Result> getResultats() {
        return resultats;
    }

    public void setResultats(List<Result> resultats) {
        this.resultats = resultats;
    }
}
