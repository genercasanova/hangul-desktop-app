package models;

import com.google.gson.annotations.SerializedName;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Classe gestió usuaris amb constructor, getters i setters.
 */
public class User {


    public SimpleIntegerProperty id;
    public SimpleStringProperty name;
    public SimpleStringProperty surname;
    public SimpleStringProperty email ;
    public SimpleStringProperty password ;
    public SimpleStringProperty role ;

    /**
     *@author Gener Casanova
     * @param id
     * @param name
     * @param surname
     * @param email
     * @param role
     */
    public User(int id, String name, String surname, String email, String role) {
        this.id=new SimpleIntegerProperty(id);
        this.name=new SimpleStringProperty(name);
        this.surname=new SimpleStringProperty(surname);
        this.email=new SimpleStringProperty(email);
        this.role=new SimpleStringProperty(role);
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public void setSurname(String surname) {
        this.surname.set(surname);
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public void setRole(String role) {
        this.role.set(role);
    }

    public Integer getId() {
        return id.get();
    }

    public String getName() {
        return name.get();
    }

    public String getSurname() {
        return surname.get();
    }

    public String getEmail() {
        return email.get();
    }

    public String getPassword() {
        return password.get();
    }


    public String getRole() {
        return role.get();
    }



}
