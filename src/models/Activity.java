package models;

public class Activity {

    int id;
    int create_date;
    int type;
    int owner_id;
    String options;
    int solution;

    public Activity(int id, int create_date, int type, int owner_id, String options, int solution) {
        this.id = id;
        this.create_date = create_date;
        this.type = type;
        this.owner_id = owner_id;
        this.options = options;
        this.solution = solution;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCreate_date() {
        return create_date;
    }

    public void setCreate_date(int create_date) {
        this.create_date = create_date;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(int owner_id) {
        this.owner_id = owner_id;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public int getSolution() {
        return solution;
    }

    public void setSolution(int solution) {
        this.solution = solution;
    }
}
