package models;

public class StatsActivity {
    int id;
    int user_id;
    int activity_id;
    int points;
    int num_errors;

    public StatsActivity(int id, int user_id, int activity_id, int points, int num_errors) {
        this.id = id;
        this.user_id = user_id;
        this.activity_id = activity_id;
        this.points = points;
        this.num_errors = num_errors;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getActivity_id() {
        return activity_id;
    }

    public void setActivity_id(int activity_id) {
        this.activity_id = activity_id;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getNum_errors() {
        return num_errors;
    }

    public void setNum_errors(int num_errors) {
        this.num_errors = num_errors;
    }
}
