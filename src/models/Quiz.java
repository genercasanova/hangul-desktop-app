package models;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;



public class Quiz {


    int student_id;
    Date date;
    int professor_id;
    String description;
    ArrayList<Question>questions;

    /**
     * @author Gener Casanova
     * @param student_id
     * @param date
     * @param professor_id
     * @param description
     * @param questions
     */
    public Quiz(int student_id, Timestamp date,int professor_id, String description,  ArrayList<Question> questions) {
        this.professor_id = professor_id;
        this.student_id = student_id;
        this.date = date;
        this.description= description;
        this.questions = questions;
    }

    public int getProfessorId() {
        return professor_id;
    }

    public void setProfessorId(int professorId) {
        this.professor_id = professor_id;
    }

    public int getStudentId() {
        return student_id;
    }

    public void setStudentId(int studentId) {
        this.student_id= student_id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String enunciat) {
        this.description = description;
    }


    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }
}
