package models;

/**
 * Classe Question, amb constructors getters i setters.
 */
public class Question {

    int option_1;
    int option_2;
    int option_3;
    int correct;

    /**
     *@author Gener Casanova
     * @param lletra_1
     * @param lletra_2
     * @param lletra_3
     * @param correct
     */
    public Question(int lletra_1, int lletra_2, int lletra_3, int correct) {
        this.option_1 = lletra_1;
        this.option_2 = lletra_2;
        this.option_3 = lletra_3;
        this.correct = correct;
    }

    public int getOption_1() {
        return option_1;
    }

    public void setOption_1(int option_1) {
        this.option_1 = option_1;
    }

    public int getOption_2() {
        return option_2;
    }

    public void setOption_2(int option_2) {
        this.option_2 = option_2;
    }

    public int getOption_3() {
        return option_3;
    }

    public void setOption_3(int option_3) {
        this.option_3 = option_3;
    }

    public int getCorrect() {
        return correct;
    }

    public void setCorrect(int correct) {
        this.correct = correct;
    }
}