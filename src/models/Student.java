package models;

public class Student {
    int id;
    int user_id;
    int professor_id;
    int final_score;
    String created_at;
    String updated_at;
    User_combobox user;

    /**
     * @param id
     * @param user_id
     * @param professor_id
     * @param final_score
     * @param created_at
     * @param updated_at
     * @param user
     */
    public Student(int id, int user_id, int professor_id, int final_score, String created_at, String updated_at, User_combobox user) {
        this.id = id;
        this.user_id = user_id;
        this.professor_id = professor_id;
        this.final_score = final_score;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.user = user;
    }

    public Student(){

    }

    public int getId() {
        return id;
    }



    public void setId(int id) {
        this.id=id;
    }

    public int getUser_id() {
        return user_id;
    }



    public void setUser_id(int user_id) {
        this.user_id=user_id;
    }

    public int getProfessor_id() {
        return professor_id;
    }



    public void setProfessor_id(int professor_id) {
        this.professor_id=professor_id;
    }

    public int getFinal_score() {
        return final_score;
    }



    public void setFinal_score(int final_score) {
        this.final_score=final_score;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public User_combobox getUser() {
        return user;
    }

    public void setUser(User_combobox user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return this.user.getName()+" "+this.user.getSurname();
    }
}
