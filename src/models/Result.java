package models;

public class Result {
    Boolean result;
    int idLetter;

    public Result(Boolean result, int idLetter) {
        this.result = result;
        this.idLetter = idLetter;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public int getIdLetter() {
        return idLetter;
    }

    public void setIdLetter(int idLetter) {
        this.idLetter = idLetter;
    }
}
