package models;

public class QuestionResponses {
    int id;
    AlphabetLetter option_1;
    AlphabetLetter option_2;
    AlphabetLetter option_3;
    int correct;
    int answer;

    /**
     *
     */
    public QuestionResponses() {
    }

    /**
     *@author Gener Casanova
     * @param id
     * @param option_1
     * @param option_2
     * @param option_3
     * @param correct
     * @param answer
     */
    public QuestionResponses(int id, AlphabetLetter option_1, AlphabetLetter option_2, AlphabetLetter option_3, int correct, int answer) {
        this.id = id;
        this.option_1 = option_1;
        this.option_2 = option_2;
        this.option_3 = option_3;
        this.correct = correct;
        this.answer = answer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public AlphabetLetter getOption_1() {
        return option_1;
    }

    public void setOption_1(AlphabetLetter option_1) {
        this.option_1 = option_1;
    }

    public AlphabetLetter getOption_2() {
        return option_2;
    }

    public void setOption_2(AlphabetLetter option_2) {
        this.option_2 = option_2;
    }

    public AlphabetLetter getOption_3() {
        return option_3;
    }

    public void setOption_3(AlphabetLetter option_3) {
        this.option_3 = option_3;
    }

    public int getCorrect() {
        return correct;
    }

    public void setCorrect(int correct) {
        this.correct = correct;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }
}
