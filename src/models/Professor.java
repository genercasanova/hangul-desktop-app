package models;

import com.google.gson.annotations.SerializedName;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Date;

public class Professor {

    int id;
    int user_id;
    String classrom;
    String presentation;
    String created_at;
    String updated_at;
    User_combobox user;



    /**
     *@author Gener Casanova
     * @param id
     * @param user_id
     * @param classrom
     * @param presentation
     * @param user
     */
    public Professor(int id, int user_id, String classrom, String presentation,String created_at, String updated_at, User_combobox user) {
        this.id = id;
        this.user_id = user_id;
        this.classrom = classrom;
        this.presentation = presentation;
        this.created_at=created_at;
        this.updated_at=updated_at;
        this.user = user;

    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getClassrom() {
        return classrom;
    }

    public void setClassrom(String classrom) {
        this.classrom = classrom;
    }

    public String getPresentation() {
        return presentation;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public User_combobox getUser() {
        return user;
    }

    public void setUser(User_combobox user) {
        this.user = user;
    }


}
