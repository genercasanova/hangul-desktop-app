package models;

import java.util.ArrayList;
import java.util.Date;

public class QuizResponses {

    int student_id;
    Date date;
    int professor_id;
    String description;
    Gradebook gradebook;
    ArrayList<QuestionResponses> questions;

    /**
     *
     */
    public QuizResponses() {
    }

    /**
     * @author Gener Casanova
     * @param student_id
     * @param date
     * @param professor_id
     * @param description
     * @param gradebook
     * @param questions
     */
    public QuizResponses(int student_id, Date date, int professor_id, String description, Gradebook gradebook, ArrayList<QuestionResponses> questions) {
        this.student_id = student_id;
        this.date = date;
        this.professor_id = professor_id;
        this.description = description;
        this.gradebook = gradebook;
        this.questions = questions;
    }

    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getProfessor_id() {
        return professor_id;
    }

    public void setProfessor_id(int professor_id) {
        this.professor_id = professor_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Gradebook getGradebook() {
        return gradebook;
    }

    public void setGradebook(Gradebook gradebook) {
        this.gradebook = gradebook;
    }

    public ArrayList<QuestionResponses> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<QuestionResponses> questions) {
        this.questions = questions;
    }
}
