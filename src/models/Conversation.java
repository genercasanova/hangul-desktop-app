package models;

import java.util.Date;

public class Conversation {

    int iduser;
    int from_id;
    int to_id;
    Date date;
    String body;

    public Conversation(int iduser, int from_id, int to_id, Date date, String body) {
        this.iduser = iduser;
        this.from_id = from_id;
        this.to_id = to_id;
        this.date = date;
        this.body = body;
    }

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    public int getFrom_id() {
        return from_id;
    }

    public void setFrom_id(int from_id) {
        this.from_id = from_id;
    }

    public int getTo_id() {
        return to_id;
    }

    public void setTo_id(int to_id) {
        this.to_id = to_id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
