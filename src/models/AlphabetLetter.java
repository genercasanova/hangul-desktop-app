package models;

public class AlphabetLetter {
   int id;
   String letter;
   String translation;
   int isVowel;
   String description;

    /**
     *
     */
    public AlphabetLetter() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public int getIsVowel() {
        return isVowel;
    }

    public void setIsVowel(int isVowel) {
        this.isVowel = isVowel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @param id
     * @param letter
     * @param translation
     * @param isVowel
     * @param description
     */

    public AlphabetLetter (int id, String letter, String translation, int isVowel, String description) {
        this.id=id;
        this.letter=letter;
        this.translation= translation;
        this.isVowel=isVowel;
        this.description=description;


    }




}