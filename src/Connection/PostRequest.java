package Connection;

import controladors.LoginController;

import java.io.*;
import java.net.*;

public class PostRequest {
    private URL url;
    String data;

    /**
     * Funció per realitzar la petició POST.
     * @param url en String per realitzar la petició.
     * @throws MalformedURLException
     */
    public PostRequest(String url) throws MalformedURLException{
        this.url = new URL(url);
        data="";
    }

    /**
     * Serialitza dades que s'envien amb la petició post.

     * @param propiedad
     * @param valor
     * @throws UnsupportedEncodingException
     */
    public void add (String propiedad, String valor) throws UnsupportedEncodingException{
//codificamos cada uno de los valores
        if (data.length()>0)
            data+= "&"+ URLEncoder.encode(propiedad, "UTF-8")+ "=" +URLEncoder.encode(valor, "UTF-8");
        else
            data+= URLEncoder.encode(propiedad, "UTF-8")+ "=" +URLEncoder.encode(valor, "UTF-8");
    }

    /**
     * Rep la resposta del servidor i la imprimeix per pantalla.
     * @return String
     * @throws IOException
     */
    public String getRespuesta() throws IOException {
        String respuesta = "";

        HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
        conexion.setRequestMethod("POST");
        conexion.setRequestProperty("Accept", "application/json");
        conexion.setRequestProperty("connection", "Keep-Alive");
        conexion.setRequestProperty("Authorization", "Bearer "+ LoginController.token);
        conexion.setDoOutput(true);
        System.out.println(LoginController.token);
        OutputStreamWriter wr = new OutputStreamWriter(conexion.getOutputStream());
        wr.write(data);
        wr.close();
        BufferedReader rd = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
        String linea;
        while ((linea = rd.readLine()) != null) {
            respuesta+= linea;
        }
        return respuesta;
    }

    /**
     * Rep la resposta del servidor i la imprimeix per pantalla és igual quel'anterior però sense passar el token
     * @return String
     * @throws IOException
     */
    public String getRespuestaLogin() throws IOException {
        String respuesta = "";

        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Accept", "application/json");
        conn.setRequestProperty("connection", "Keep-Alive");
        conn.setDoOutput(true);
        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
        wr.write(data);
        wr.close();
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String linea;
        while ((linea = rd.readLine()) != null) {
            respuesta+= linea;
        }
        return respuesta;
    }

    /**
     * Igual que els anteriors però pujant el document json.
     * @param json
     * @return
     * @throws IOException
     */
    public String getRespuestaQuiz(String json) throws IOException {
        String respuesta = "";
        data=json;

        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Accept", "application/json");
        conn.setRequestProperty("connection", "Keep-Alive");
        conn.setRequestProperty("Authorization", "Bearer "+ LoginController.token);
        conn.setDoOutput(true);
        System.out.println(data);
    OutputStream os = conn.getOutputStream();
    os.write(data.getBytes());
    os.flush();
    os.close();
    System.out.println(data);
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String linea;
        while ((linea = rd.readLine()) != null) {
            respuesta+= linea;
        }
        return respuesta;
    }

}

