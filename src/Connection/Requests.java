package Connection;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import controladors.LoginController;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import models.AlphabetLetter;
import models.Question;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Requests {

    /**
     *
     */
    @FXML
private Label soundsLike;

    public Requests() throws Exception {
    }

    public static String PeticioGet(String urlToRead) throws Exception {
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlToRead);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("Accept", "application/json");
        conn.setRequestProperty("connection", "Keep-Alive");
        conn.setRequestProperty("Authorization", "Bearer " + LoginController.token);
        conn.setRequestMethod("GET");
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        return result.toString();
    }

    public void deleteUser(String user) throws IOException {

        URL url = null;
        try {
            url = new URL("https://work.maximilianofernandez.net/api/users/" + user + "}");

        } catch (MalformedURLException exception) {
            exception.printStackTrace();
        }
        HttpURLConnection conexion = null;
        try {
            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestProperty("Accept", "application/json");
            conexion.setRequestProperty("connection", "Keep-Alive");
            conexion.setRequestProperty("Authorization", "Bearer " + LoginController.token);
            conexion.setRequestMethod("DELETE");
            System.out.println(conexion.getResponseCode());
        } catch (IOException exception) {
            exception.printStackTrace();
        } finally {
            if (conexion != null) {
                conexion.disconnect();
            }

        }
    }

    public void tancarLaSessio() throws Exception {
        String sessio = PeticioGet("https://work.maximilianofernandez.net/api/logout");
        System.out.println(sessio);

    }

    public static ArrayList<AlphabetLetter> readJson() throws Exception {
        GetRequest req = new GetRequest();
        String response = req.PeticioGet("https://work.maximilianofernandez.net/api/hangul");

        System.out.println(response);
        Gson gson = new Gson();
        ArrayList<AlphabetLetter> alpha = gson.fromJson(response, new TypeToken<List<AlphabetLetter>>() {
        }.getType());

        for (AlphabetLetter alphabetLetter : alpha) {
            System.out.println(alphabetLetter);
        }
        return alpha;
    }

    public static Question guardarPreguntes(String lletra1, String lletra2, String lletra3) throws Exception {
        ArrayList<AlphabetLetter> lletres = readJson();
        int primera=0;
        int segona=0;
        int tercera=0;
        int bona = 1;
        for (AlphabetLetter miObjeto : lletres) {
            if (miObjeto.getLetter().equalsIgnoreCase(lletra1)) {
                primera = miObjeto.getId();
            } else if (miObjeto.getLetter().equalsIgnoreCase(lletra2)) {
                segona = miObjeto.getId();
            } else if (miObjeto.getLetter().equalsIgnoreCase(lletra3)) {
                tercera = miObjeto.getId();
            }
        }
        System.out.println("Enhorabuena! Lo has encontrado!");
        Question question= new Question(primera,segona, tercera,bona);
        System.out.println(question.getOption_1());
        System.out.println(question.getOption_2());
        System.out.println(question.getOption_3());
        System.out.println(question.getCorrect());
        return question;
    }









    public void getUsers() throws IOException {

}}


