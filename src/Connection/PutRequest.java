package Connection;

import controladors.LoginController;

import java.io.*;
import java.net.*;

public class PutRequest {
    private URL url;
    String data;

    public PutRequest(String url) throws MalformedURLException{
        this.url = new URL(url);
        data="";
    }

    public void add (String propiedad, String valor) throws UnsupportedEncodingException{

        if (data.length()>0)
            data+= "&"+ URLEncoder.encode(propiedad, "UTF-8")+ "=" +URLEncoder.encode(valor, "UTF-8");
        else
            data+= URLEncoder.encode(propiedad, "UTF-8")+ "=" +URLEncoder.encode(valor, "UTF-8");
    }

    /**
     * Després fa la petició amb el mètode put i la resposta la imprimeix per pantalla.
     * @return
     * @throws IOException
     */
    public String getRespuesta() throws IOException {
        String respuesta = "";

        HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
        conexion.setRequestMethod("PUT");
        conexion.setRequestProperty("Accept", "application/json");
        conexion.setRequestProperty("connection", "Keep-Alive");
        conexion.setRequestProperty("Authorization", "Bearer "+ LoginController.token);
        conexion.setDoOutput(true);
        System.out.println(LoginController.token);
        OutputStreamWriter wr = new OutputStreamWriter(conexion.getOutputStream());
        wr.write(data);
        wr.close();
        BufferedReader rd = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
    String linea;
        while ((linea = rd.readLine()) != null) {
        respuesta+= linea;
    }
        return respuesta;
}




}


